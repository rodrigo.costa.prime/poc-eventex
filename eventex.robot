*** Settings ***
Suite Setup       Abrir Navegador
Suite Teardown    Fechar Navegador

Library  SeleniumLibrary

*** Variables ***
${URL}                  https://eventex-ronaldooliveira.herokuapp.com/
${BROWSER}              chrome
${SUBSCRIBE_BUTTON}     Inscreva-se já
${NAME_INPUT}           id = id_name
${CPF_INPUT}            id = id_cpf
${EMAIL_INPUT}          id = id_email
${PHONE_INPUT}          id = id_phone
${SEND_BUTTON}          Enviar!
${SUCESS_MESSAGE}       Obrigada por se inscrever!

*** Test Cases ***
Cenário: Inscrever-se para participar
   Dado que o usuario queira se registrar para o evento
   Quando realizar seu cadastro
   Entao deve visualizar a confirmacao de inscricao

*** Keywords ***
Abrir Navegador
   Open Browser  ${URL}            ${BROWSER}

Dado que o usuario queira se registrar para o evento
   Click Link    ${SUBSCRIBE_BUTTON}

Quando realizar seu cadastro
   Press Keys    ${NAME_INPUT}     Fulano de Tal
   Press Keys    ${CPF_INPUT}      12345678900
   Press Keys    ${EMAIL_INPUT}    fake@mail.com
   Press Keys    ${PHONE_INPUT}    11 987654321
   Click Button  ${SEND_BUTTON}

Entao deve visualizar a confirmacao de inscricao
   Page Should Contain             ${SUCESS_MESSAGE}
   sleep  5s

Fechar Navegador
   Close browser